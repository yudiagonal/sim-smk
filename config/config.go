package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/yudiagonal/sim-smk/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB
var err error

func ConnectDB() (*gorm.DB, error) {
	if err = godotenv.Load(); err != nil {
		log.Panic(err)
	}
	dsn := os.Getenv("dbUser") + ":" + os.Getenv("dbPass") + "@tcp(" + os.Getenv("dbHost") + ":" + os.Getenv("dbPort") + ")/" + os.Getenv("dbName") + "?charset=utf8&parseTime=true&loc=Local"
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{SkipDefaultTransaction: true})
	if err != nil {
		panic("failed to connect database")
	}
	err = DB.AutoMigrate(&models.Guru{})
	if err != nil {
		log.Println("Migration failed")
	}

	return DB, nil
}
