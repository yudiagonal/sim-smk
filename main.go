package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/yudiagonal/sim-smk/config"
)

func main() {
	config.ConnectDB()
	http.HandleFunc("/index", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("cok")
	})

	fmt.Println("server started at localhost:9000")
	log.Fatal(http.ListenAndServe(":9000", nil))
}
