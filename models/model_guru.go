package models

type Guru struct {
	NikGuru     string `gorm:"size:36;not null;uniqueIndex; primary_key; validate:required"`
	NamaGuru    string `gorm:"size:255;not null;validate:required"`
	TempatLahir string `gorm:"size:100;not null;validate:required"`
	NoUPTK      string `gorm:"size:100;not null;validate:required"`
}
